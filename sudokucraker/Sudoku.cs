﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace SudokuCraker
{
    /// <summary>
    /// sudoku grid with rows 1-9 and columns 1-9
    /// </summary>
    class Sudoku
    {
        /// We always do (row, column) to acces the values.
        private int[,] cellValues = new int[9, 9];
        private List<int[]> inmovableIndex = new List<int[]>();

        public Sudoku(int[,] values)
        {
            cellValues = values;
        }
        public Sudoku()
        {
        }

        /// <summary>
        /// create a new sudoku objectc with the same starting values as an original
        /// </summary>
        /// <param name="mainSudoku"></param>
        public Sudoku(Sudoku mainSudoku)
        {
            for (int row = 0; row < mainSudoku.GetRowLength(); row++)
            {
                for (int column = 0; column < mainSudoku.GetColumnLength(); column++)
                {
                    cellValues[row, column] = mainSudoku.cellValues[row, column];
                }
            }
        }

        public int GetRowLength()
        {
            return cellValues.GetLength(0);
        }
        public int GetColumnLength()
        {
            return cellValues.GetLength(1);
        }

        /// <summary>
        /// assign provided values to the row, ignoring inmovable cells
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="values"></param>
        internal void setMissingCellsInRow(int rowIndex, int[] values)
        {
            int currentColumn = 0;
            for (int valueIndex = 0; valueIndex < values.Length; valueIndex++)
            {
                // find the next missing cell
                while(IndexIsInmovable(rowIndex, currentColumn) && currentColumn < GetRowLength()) 
                {
                    currentColumn++;
                }
                if (currentColumn < GetRowLength())
                {
                    cellValues[rowIndex, currentColumn] = values[valueIndex];
                    currentColumn++;
                }
            }
        }

        public void SetCurrentCellsAsInmovable()
        {
            for (int row = 0; row < GetRowLength(); row++)
            {
                for (int column = 0; column < GetColumnLength(); column++)
                {
                    if (cellValues[row, column] > 0)
                    {
                        SetInmovableCell(row, column);
                    }
                }

            }
        }

        /// <summary>
        /// set a cell to be unchangable. cell count goes from 0-8
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        public void SetInmovableCell( int row, int column)
        {
            int[] index = new int[] { row, column };
            if (!inmovableIndex.Contains(index))
            {
                inmovableIndex.Add(index);
            }
        }

        public void SetInmovableCell(int row, int column, int value)
        {
            int[] index = new int[] { row , column };
            if (!inmovableIndex.Contains(index))
            {
                inmovableIndex.Add(index);
                cellValues[row, column] = value;
            }
        }

        

        /// <summary>
        /// returns a list of indexes that have been set to inmovable
        /// </summary>
        public List<int[]> InmovableIndex 
        {
            get
            {
                return inmovableIndex;
            }
         }

        #region row props
        public int[] Row0 { 
            get 
            {
                return new int[] { cellValues[0, 0], cellValues[0, 1], cellValues[0, 2],
                cellValues[0, 3], cellValues[0, 4], cellValues[0, 5], cellValues[0, 6],
                cellValues[0, 7], cellValues[0, 8]};
            } 
            set
            {
                int currentRow = 0;
                SetRow(value, currentRow);
            }                            
        }
        public int[] Row1
        {
            get
            {
                return new int[] { cellValues[1, 0], 
                                   cellValues[1, 1], 
                                   cellValues[1, 2],
                                   cellValues[1, 3],
                                   cellValues[1, 4],
                                   cellValues[1, 5], 
                                   cellValues[1, 6],
                                   cellValues[1, 7], 
                                   cellValues[1, 8]};
            }
            set
            {
                int currentRow = 1;
                SetRow(value, currentRow);
            }
        }
        public int[] Row2
        {
            get
            {
                return new int[] { cellValues[2, 0],
                                   cellValues[2, 1],
                                   cellValues[2, 2],
                                   cellValues[2, 3],
                                   cellValues[2, 4],
                                   cellValues[2, 5],
                                   cellValues[2, 6],
                                   cellValues[2, 7],
                                   cellValues[2, 8]};
            }
            set
            {
                int currentRow = 2;
                SetRow(value, currentRow);
            }
        }
        public int[] Row3
        {
            get
            {
                return new int[] { cellValues[3, 0],
                                   cellValues[3, 1],
                                   cellValues[3, 2],
                                   cellValues[3, 3],
                                   cellValues[3, 4],
                                   cellValues[3, 5],
                                   cellValues[3, 6],
                                   cellValues[3, 7],
                                   cellValues[3, 8]};
            }
            set
            {
                int currentRow = 3;
                SetRow(value, currentRow);
            }
        }
        public int[] Row4
        {
            get
            {
                return new int[] { cellValues[4, 0],
                                   cellValues[4, 1],
                                   cellValues[4, 2],
                                   cellValues[4, 3],
                                   cellValues[4, 4],
                                   cellValues[4, 5],
                                   cellValues[4, 6],
                                   cellValues[4, 7],
                                   cellValues[4, 8]};
            }
            set
            {
                int currentRow = 4;
                SetRow(value, currentRow);
            }
        }
        public int[] Row5
        {
            get
            {
                return new int[] { cellValues[5, 0],
                                   cellValues[5, 1],
                                   cellValues[5, 2],
                                   cellValues[5, 3],
                                   cellValues[5, 4],
                                   cellValues[5, 5],
                                   cellValues[5, 6],
                                   cellValues[5, 7],
                                   cellValues[5, 8]};
            }
            set
            {
                int currentRow = 5;
                SetRow(value, currentRow);
            }
        }
        public int[] Row6
        {
            get
            {
                return new int[] { cellValues[6, 0],
                                   cellValues[6, 1],
                                   cellValues[6, 2],
                                   cellValues[6, 3],
                                   cellValues[6, 4],
                                   cellValues[6, 5],
                                   cellValues[6, 6],
                                   cellValues[6, 7],
                                   cellValues[6, 8]};
            }
            set
            {
                int currentRow = 6;
                SetRow(value, currentRow);
            }
        }
        public int[] Row7
        {
            get
            {
                return new int[] { cellValues[7, 0],
                                   cellValues[7, 1],
                                   cellValues[7, 2],
                                   cellValues[7, 3],
                                   cellValues[7, 4],
                                   cellValues[7, 5],
                                   cellValues[7, 6],
                                   cellValues[7, 7],
                                   cellValues[7, 8]};
            }
            set
            {
                int currentRow = 7;
                SetRow(value, currentRow);
            }
        }
        public int[] Row8
        {
            get
            {
                return GetRow(8);
            }
            set
            {
                int currentRow = 8;
                SetRow(value, currentRow);
            }
        }
        #endregion

        #region column props
        public int[] Col0
        {
            get
            {
                return new int[] { cellValues[0, 0],
                                   cellValues[1, 0],
                                   cellValues[2, 0],
                                   cellValues[3, 0],
                                   cellValues[4, 0],
                                   cellValues[5, 0],
                                   cellValues[6, 0],
                                   cellValues[7, 0],
                                   cellValues[8, 0]};
            }
            set
            {
                int currentRow = 0;
                SetColumn(value, currentRow);
            }
        }
        public int[] Col1
        {
            get
            {
                return new int[] { cellValues[0, 1],
                                   cellValues[1, 1],
                                   cellValues[2, 1],
                                   cellValues[3, 1],
                                   cellValues[4, 1],
                                   cellValues[5, 1],
                                   cellValues[6, 1],
                                   cellValues[7, 1],
                                   cellValues[8, 1]};
            }
            set
            {
                int currentRow = 1;
                SetColumn(value, currentRow);
            }
        }
        public int[] Col2
        {
            get
            {
                return new int[] { cellValues[0, 2],
                                   cellValues[1, 2],
                                   cellValues[2, 2],
                                   cellValues[3, 2],
                                   cellValues[4, 2],
                                   cellValues[5, 2],
                                   cellValues[6, 2],
                                   cellValues[7, 2],
                                   cellValues[8, 2]};
            }
            set
            {
                int currentRow = 2;
                SetColumn(value, currentRow);
            }
        }
        public int[] Col3
        {
            get
            {
                return new int[] { cellValues[0, 3],
                                   cellValues[1, 3],
                                   cellValues[2, 3],
                                   cellValues[3, 3],
                                   cellValues[4, 3],
                                   cellValues[5, 3],
                                   cellValues[6, 3],
                                   cellValues[7, 3],
                                   cellValues[8, 3]};
            }
            set
            {
                int currentRow = 3;
                SetColumn(value, currentRow);
            }
        }
        public int[] Col4
        {
            get
            {
                return new int[] { cellValues[0, 4],
                                   cellValues[1, 4],
                                   cellValues[2, 4],
                                   cellValues[3, 4],
                                   cellValues[4, 4],
                                   cellValues[5, 4],
                                   cellValues[6, 4],
                                   cellValues[7, 4],
                                   cellValues[8, 4]};
            }
            set
            {
                int currentRow = 4;
                SetColumn(value, currentRow);
            }
        }
        public int[] Col5
        {
            get
            {
                return new int[] { cellValues[0, 5],
                                   cellValues[1, 5],
                                   cellValues[2, 5],
                                   cellValues[3, 5],
                                   cellValues[4, 5],
                                   cellValues[5, 5],
                                   cellValues[6, 5],
                                   cellValues[7, 5],
                                   cellValues[8, 5]};
            }
            set
            {
                int currentRow = 5;
                SetColumn(value, currentRow);
            }
        }
        public int[] Col6
        {
            get
            {
                return new int[] { cellValues[0, 6],
                                   cellValues[1, 6],
                                   cellValues[2, 6],
                                   cellValues[3, 6],
                                   cellValues[4, 6],
                                   cellValues[5, 6],
                                   cellValues[6, 6],
                                   cellValues[7, 6],
                                   cellValues[8, 6]};
            }
            set
            {
                int currentRow = 6;
                SetColumn(value, currentRow);
            }
        }
        public int[] Col7
        {
            get
            {
                return new int[] { cellValues[0, 7],
                                   cellValues[1, 7],
                                   cellValues[2, 7],
                                   cellValues[3, 7],
                                   cellValues[4, 7],
                                   cellValues[5, 7],
                                   cellValues[6, 7],
                                   cellValues[7, 7],
                                   cellValues[8, 7]};
            }
            set
            {
                int currentRow = 7;
                SetColumn(value, currentRow);
            }
        }
        public int[] Col8
        {
            get
            {
                return new int[] { cellValues[0, 8],
                                   cellValues[1, 8],
                                   cellValues[2, 8],
                                   cellValues[3, 8],
                                   cellValues[4, 8],
                                   cellValues[5, 8],
                                   cellValues[6, 8],
                                   cellValues[7, 8],
                                   cellValues[8, 8]};
            }
            set
            {
                int currentColumn = 8;
                SetColumn(value, currentColumn);
            }
        }
        #endregion

        #region box
        public int[] box1 
        { 
            get
            {
                return new int[] { cellValues[0, 0], cellValues[0, 1], cellValues[0, 2],
                                   cellValues[1, 0], cellValues[1, 1], cellValues[1, 2],
                                   cellValues[2, 0], cellValues[2, 1], cellValues[2, 2]
                };
            } 
        }
        public int[] box2
        {
            get
            {
                return new int[] { cellValues[0, 3], cellValues[0, 4], cellValues[0, 5],
                                   cellValues[1, 3], cellValues[1, 4], cellValues[1, 5],
                                   cellValues[2, 3], cellValues[2, 4], cellValues[2, 5]
                };
            }
        }
        public int[] box3 
        {
            get
            {
                return new int[] { cellValues[0, 6], cellValues[0, 7], cellValues[0, 8],
                                   cellValues[1, 6], cellValues[1, 7], cellValues[1, 8],
                                   cellValues[2, 6], cellValues[2, 7], cellValues[2, 8]
                };
            }
        }
        public int[] box4
        {
            get
            {
                return new int[] { cellValues[3, 0], cellValues[3, 1], cellValues[3, 2],
                                   cellValues[4, 0], cellValues[4, 1], cellValues[4, 2],
                                   cellValues[5, 0], cellValues[5, 1], cellValues[5, 2]
                };
            }
        }
        public int[] box5
        {
            get
            {
                return new int[] { cellValues[3, 3], cellValues[3, 4], cellValues[3, 5],
                                   cellValues[4, 3], cellValues[4, 4], cellValues[4, 5],
                                   cellValues[5, 3], cellValues[5, 4], cellValues[5, 5]
                };
            }
        }
        public int[] box6
        {
            get
            {
                return new int[] { cellValues[3, 6], cellValues[3, 7], cellValues[3, 8],
                                   cellValues[4, 6], cellValues[4, 7], cellValues[4, 8],
                                   cellValues[5, 6], cellValues[5, 7], cellValues[5, 8]
                };
            }
        }
        public int[] box7
        {
            get
            {
                return new int[] { cellValues[6, 0], cellValues[6, 1], cellValues[6, 2],
                                   cellValues[7, 0], cellValues[7, 1], cellValues[7, 2],
                                   cellValues[8, 0], cellValues[8, 1], cellValues[8, 2]
                };
            }
        }
        public int[] box8
        {
            get
            {
                return new int[] { cellValues[6, 3], cellValues[6, 4], cellValues[6, 5],
                                   cellValues[7, 3], cellValues[7, 4], cellValues[7, 5],
                                   cellValues[8, 3], cellValues[8, 4], cellValues[8, 5]
                };
            }
        }
        public int[] box9
        {
            get
            {
                return new int[] { cellValues[6, 6], cellValues[6, 7], cellValues[6, 8],
                                   cellValues[7, 6], cellValues[7, 7], cellValues[7, 8],
                                   cellValues[8, 6], cellValues[8, 7], cellValues[8, 8]
                };
            }
        }
        #endregion
        public int[][] GetColumns()
        {
            int[][] columns = { Col0, Col1, Col2, Col3, Col4, Col5, Col6, Col7, Col8 };
            return columns;
        }
        public int[][] GetRows()
        {
            int[][] columns = { Row0, Row1, Row2, Row2, Row5, Row5, Row6, Row7, Row8 };
            return columns;
        }
        public int[][] GetBoxen()
        {
            int[][] boxen = { box1, box2, box3, box4, box5, box6, box7, box8, box9 };
            return boxen;
        }

        private void SetColumn(int[] values, int currentColumn)
        {
            for (int currentRow = 0; currentRow < values.Length; currentRow++)
            {  
                if (!IndexIsInmovable(currentRow, currentColumn))
                {
                    cellValues[currentRow, currentColumn] = values[currentRow];
                }
            }
        }

        private void SetRow(int[] values, int currentRow)
        {
            for (int currentColumn = 0; currentColumn < values.Length; currentColumn++)
            {
                if (!IndexIsInmovable(currentRow, currentColumn))
                {
                    cellValues[currentRow, currentColumn] = values[currentColumn];
                }
            }
        }

        public int[] GetRow(int rowIndex)
        {
            int[] row = new int[GetRowLength()];
            for (int columnIndex = 0; columnIndex < GetColumnLength(); columnIndex++)
            {
                row[columnIndex] = cellValues[rowIndex, columnIndex];
            }
            return row;
        }

        public bool IndexIsInmovable(int row, int column)
        {
            bool isInmovable = false;
            foreach (int[] item in inmovableIndex)
            {
                int inmovableRow = item[0];
                int inmovableColumn = item[1];

                if (inmovableColumn == column && inmovableRow == row)
                {
                    isInmovable = true;
                }
            }

            return isInmovable;
        }

        /// <summary>
        /// validate that the sequence is composed of unique numbers
        /// </summary>
        /// <param name="line">array of integers.</param>
        /// <returns>False if there are duplicate numbers. True if all numbers are unique.</returns>
        public bool ValidateLine(int[] line, bool ignoreZeroes=false)
        {
            for (int i = 0; i < line.Length; i++)
            {
                // ignore cells that are not set yet.
                if ( ignoreZeroes && line[i] < 1)
                {
                    continue;
                }

                // count the amount of times current number repeates
                int counts = line.Count(val => val == line[i]);
                if (counts > 1)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// validate that the whole thing is legal
        /// </summary>
        /// <returns>True if valid, False otherwise</returns>
        public bool ValidateSudoku(bool ignoreZeroes=false)
        {
            bool lineIsCorrect;
            foreach (int[] item in GetColumns())
            {
                lineIsCorrect = ValidateLine(item, ignoreZeroes);
                if (!lineIsCorrect)
                {
                    return false;
                }
            }
            foreach (int[] item in GetRows())
            {
                lineIsCorrect = ValidateLine(item, ignoreZeroes);
                if (!lineIsCorrect)
                {
                    return false;
                }
            }
            foreach(int[] item in GetBoxen())
            {
                lineIsCorrect = ValidateLine(item, ignoreZeroes);
                if (!lineIsCorrect)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
