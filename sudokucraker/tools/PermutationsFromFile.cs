﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace SudokuCraker.Tools
{
    /// <summary>
    /// Archived but kept for reference.
    /// I did IO stuff that i will want to look at later.
    /// </summary>
    class PermutationsFromFile
    {
        private const string HEADER_END_CHAR = ":";
        private const string SEQUENCE_END_CHAR = ";";
        private const string VALUE_END_CHAR = ",";
        private const string LINE_END_CHAR = "\n";
        private readonly int HEADER_END_INT; 
        private readonly int SEQUENCE_END_INT;
        private readonly int VALUE_END_INT;
        private readonly int LINE_END_INT;



        private readonly string path = @"SudokuCracker_permutations.txt";
        private ASCIIEncoding encoder = new ASCIIEncoding();

        public PermutationsFromFile()
        {

            if (!File.Exists(path))
            {
                FileStream stream = File.Create(path);
                stream.Close();
            }
            HEADER_END_INT = Convert.ToInt32(encoder.GetBytes(HEADER_END_CHAR)[0]);
            SEQUENCE_END_INT = Convert.ToInt32(encoder.GetBytes(SEQUENCE_END_CHAR)[0]);
            VALUE_END_INT = Convert.ToInt32(encoder.GetBytes(VALUE_END_CHAR)[0]);
            LINE_END_INT= Convert.ToInt32(encoder.GetBytes(LINE_END_CHAR)[0]);

        }

        /// <summary>
        /// yields the permutations of the possible values given by reading a file.
        /// this is no longer in use, but kept here as reference
        /// for using a StreamReader/Writer
        /// </summary>
        /// <param name="possibleValues"></param>
        /// <returns></returns>
        public IEnumerable<int[]> getPermutationsFromFile(int[] possibleValues)
        {
            // sort possible values
            SortedSet<int> header = new SortedSet<int>(possibleValues);
            bool calculatePerms = false;

            /// yields one permutation sequence at a time from an existing file

            // read the file looking for those permutations
            using (StreamReader fs = new StreamReader(path, encoder))
            {
                setStreamToHeader(header, fs);

               
                if (fs.Peek() >= 0)
                {
                    int peek = fs.Peek();
                    while(fs.Peek() > 0 && fs.Peek() != LINE_END_INT)
                    {
                        int[] permutation = GetPermSequenceFromFile(fs);
                        if (permutation.Any())
                        {
                            yield return permutation;
                        }
                        else
                        {
                            yield break;
                        }
                    }
                }
                else
                {
                    // end of file was reached and no header was matched
                    calculatePerms = true;
                }
                fs.Close();
            }

            if (calculatePerms)
            {
                // we need to calculate the permutations and write it to file
                CalculatePermutations(header);
                foreach (var item in getPermutationsFromFile(possibleValues))
                {
                    yield return item;
                }
            }
        }

        private void CalculatePermutations(SortedSet<int> values)
        {
            int[] valuesAsArray = values.ToArray();
            using (StreamWriter fw = new StreamWriter(path, true, encoder))
            {
                fw.Write(String.Join(VALUE_END_CHAR, values));
                fw.Write(HEADER_END_CHAR);
                ForAllPermutation(valuesAsArray, (item) => {
                    fw.Write(String.Join(VALUE_END_CHAR, item));
                    fw.Write(SEQUENCE_END_CHAR);
                    return false;
                });
                fw.WriteLine();
            }
        }

        private int[] GetPermSequenceFromFile(StreamReader fs)
        {
            List<int> returnSet = new List<int>();
            List<byte> readChar = new List<byte>();

            int buffer = fs.Peek();
            // get the next sequence.
            while (buffer >= 0 && buffer != LINE_END_INT)
            {
                // read one char
                buffer = fs.Read();
                ParseValue(readChar, buffer, returnSet);

                if (buffer == SEQUENCE_END_INT || fs.Peek() < 0 || buffer == LINE_END_INT)
                {
                    // separrator found. flush values
                    break;
                }
            }

            return returnSet.ToArray();
        }

        /// <summary>
        /// set the stream reader to the beginning of the sequence.
        /// </summary>
        /// <param name="values">header values to match</param>
        /// <param name="stream">Stream Reader to set to the correct position</param>
        private void setStreamToHeader(SortedSet<int> values, StreamReader stream)
        {
            List<byte> readChar = new List<byte>();
            int buff = 0;
            bool stopLoop = false;

            List<int> header = new List<int>();

            while (!stopLoop && stream.Peek() >= 0)
            {
                // read one char
                buff = stream.Read();
                if (buff < 0)
                {
                    stopLoop = true;
                }
                if (buff == HEADER_END_INT)
                {
                    int found_int = DecodeToInt(readChar);
                    header.Add(found_int);

                    if (header.SequenceEqual(values))
                    {
                        // found the header that matches the values given.
                        stopLoop = true;
                    }
                    else
                    {
                        // header does not match. Go to next line
                        while (buff != LINE_END_INT)
                        {
                            buff = stream.Read();
                        }
                        readChar.Clear();
                        header.Clear();
                    }

                }
                else
                {
                    ParseValue(readChar, buff, header);
                }

            }
        }

        private void ParseValue(List<byte> readChar, int buffRead, List<int> returnSet)
        {
            if (buffRead == VALUE_END_INT || buffRead == SEQUENCE_END_INT || buffRead == HEADER_END_INT)
            {
                // decode string and add to return set
                int found_int = DecodeToInt(readChar);
                returnSet.Add(found_int);
                readChar.Clear();
            }
            else
            {
                // no separation found, 
                readChar.Add(Convert.ToByte(buffRead));
            }
        }

        private int DecodeToInt(List<byte> readChar)
        {
            // we have finished discovering the value. translate it
            return Convert.ToInt32(encoder.GetString(readChar.ToArray()));
        }

        /// <summary>
        /// Heap's algorithm to find all pmermutations. Non recursive, more efficient.
        /// from https://stackoverflow.com/a/36634935/6452188
        /// </summary>
        /// <param name="items">Items to permute in each possible ways</param>
        /// <param name="funcExecuteAndTellIfShouldStop"></param>
        /// <returns>Return true if cancelled</returns> 
        public static bool ForAllPermutation<T>(T[] items, Func<T[], bool> funcExecuteAndTellIfShouldStop)
        {
            int countOfItem = items.Length;

            if (countOfItem <= 1)
            {
                return funcExecuteAndTellIfShouldStop(items);
            }

            var indexes = new int[countOfItem];
            for (int i = 0; i < countOfItem; i++)
            {
                indexes[i] = 0;
            }

            if (funcExecuteAndTellIfShouldStop(items))
            {
                return true;
            }

            for (int i = 1; i < countOfItem;)
            {
                if (indexes[i] < i)
                { // On the web there is an implementation with a multiplication which should be less efficient.
                    if ((i & 1) == 1) // if (i % 2 == 1)  ... more efficient ??? At least the same.
                    {
                        Swap(ref items[i], ref items[indexes[i]]);
                    }
                    else
                    {
                        Swap(ref items[i], ref items[0]);
                    }

                    if (funcExecuteAndTellIfShouldStop(items))
                    {
                        return true;
                    }

                    indexes[i]++;
                    i = 1;
                }
                else
                {
                    indexes[i++] = 0;
                }
            }

            return false;
        }

        private static void Swap<T>(ref T a, ref T b)
        {
            T tmp = a;
            a = b;
            b = tmp;
        }
    }

    static class PermutationCalculator
    {
       
        /// <summary>
        /// Heap's algorithm to find all pmermutations.
        /// adapted from https://stackoverflow.com/a/36634935/6452188
        /// </summary>
        /// <param name="items">Items to permute in each possible ways</param>
        /// <param name="funcExecuteAndTellIfShouldStop"></param>
        /// <returns>Return true if cancelled</returns> 
        public static IEnumerable<T[]> PermutationGenerator<T>(T[] items)
        {
            int countOfItem = items.Length;

            if (countOfItem <= 1)
            {
                yield return items;
            }

            var indexes = new int[countOfItem];
            for (int i = 0; i < countOfItem; i++)
            {
                indexes[i] = 0;
            }

            yield return items;

            for (int i = 1; i < countOfItem;)
            {
                if (indexes[i] < i)
                { // On the web there is an implementation with a multiplication which should be less efficient.
                    if ((i & 1) == 1) // if (i % 2 == 1)  ... more efficient ??? At least the same.
                    {
                        Swap(ref items[i], ref items[indexes[i]]);
                    }
                    else
                    {
                        Swap(ref items[i], ref items[0]);
                    }

                    yield return items;

                    indexes[i]++;
                    i = 1;
                }
                else
                {
                    indexes[i++] = 0;
                }
            }

            yield break;
        }
        private static void Swap<T>(ref T a, ref T b)
        {
            T tmp = a;
            a = b;
            b = tmp;
        }

    }


}
