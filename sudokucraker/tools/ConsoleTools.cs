﻿using System;

namespace SudokuCraker.Tools
{
    public class ConsoleTools
    {
        public static void WriteSudokuLine(int[] line)
        {
            Console.WriteLine("[{0}]", string.Join(", ", line));
        }

    }
}