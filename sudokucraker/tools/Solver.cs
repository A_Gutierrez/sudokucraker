﻿using SudokuCraker.Tools;
using System.Collections.Generic;

namespace SudokuCraker
{
    class Solver
    {
        public Sudoku BruteForce(Sudoku sudoku)
        {
            // if it's solved already, return
            if (sudoku.ValidateSudoku())
            {
                return sudoku;
            }

            // start at row 0, and recuse to row 8
            int rowIndex = 0;
            RecursiveSolveRow(sudoku, rowIndex, false);

            return sudoku;
        }

        /// <summary>
        /// goes to the last row and applies all permutations, validating each time
        /// then it goes to the previous, applies next permutations and repeats for each row.
        /// </summary>
        /// <param name="sudoku"></param>
        /// <param name="RowIndex"></param>
        private void RecursiveSolveRow(Sudoku sudoku, int RowIndex, bool stopIteration)
        {
            if(stopIteration) { return; }

            int[] current_row = sudoku.GetRows()[RowIndex];
            foreach (var missingValues in GetRowGenerator(sudoku, RowIndex, current_row))
            {
                sudoku.setMissingCellsInRow(RowIndex, missingValues);
                // generate perms for next row and  excecute all of them
                if (RowIndex + 1 < sudoku.GetRowLength()) // stop at max depth
                {
                    RecursiveSolveRow(sudoku, RowIndex + 1, stopIteration);
                }
                stopIteration = sudoku.ValidateSudoku();
                if (stopIteration)
                {
                    return;
                }

            }
        }

        /// <summary>
        /// returns a generator that yields the permutations of the
        /// available numbers per row.
        /// </summary>
        /// <param name="sudoku">sudoku obje</param>
        /// <param name="rowNumber"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private IEnumerable<int[]> GetRowGenerator(Sudoku sudoku, int rowNumber, int[] row)
        {
            List<int> avoidNumbers = new List<int>();
            foreach (int[] item in sudoku.InmovableIndex)
            {
                if (item[0] == rowNumber)
                {
                    int inmovableValue = row[item[1]];
                    avoidNumbers.Add(inmovableValue);
                }
            }
            // create an array of numbers to permute
            List<int> permuteHeader = new List<int>();
            for (int i = 1; i <= 9; i++)
            {
                if (!avoidNumbers.Contains(i))
                {
                    permuteHeader.Add(i);
                }
            }
            return PermutationCalculator.PermutationGenerator<int>(permuteHeader.ToArray());
        }
    }


}
