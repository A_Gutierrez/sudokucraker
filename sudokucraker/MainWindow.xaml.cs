﻿using SudokuCraker.Tools;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SudokuCraker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Sudoku mainSudoku = new Sudoku();
        private Button selectedCell;
        private Brush selectedColor = Brushes.DarkGray;
        private Brush idleColor = Brushes.LightGray;
        private Brush solveColor = Brushes.Cornsilk;

        public MainWindow()
        {
            InitializeComponent();
            Button[][] allRows = GetButtonRows();

            /// set Buttons to default
            for (int row = 0; row < allRows.Length; row++)
            {
                Button[] currentRow = allRows[row];
                for (int column = 0; column < currentRow.Length; column++)
                {
                    Button button = currentRow[column];
                    button.Content = "";
                    button.Background = idleColor;
                }
            }
        }

        /// <summary>
        /// shit man this takes forever. needs more given numbers
        /// </summary>
        private static void HardSolve()
        {
            Sudoku sudoku = new Sudoku();
            Solver solver = new Solver();
            sudoku.Row0 = new int[] { 0, 0, 6, 0, 0, 9, 0, 0, 0 };
            sudoku.Row1 = new int[] { 1, 3, 0, 0, 0, 0, 0, 0, 4 };
            sudoku.Row2 = new int[] { 0, 0, 5, 0, 0, 6, 0, 0, 0 };
            sudoku.Row3 = new int[] { 0, 1, 0, 8, 7, 0, 9, 0, 0 };
            sudoku.Row4 = new int[] { 0, 0, 0, 0, 0, 0, 0, 5, 0 };
            sudoku.Row5 = new int[] { 0, 2, 9, 0, 0, 5, 3, 0, 0 };
            sudoku.Row6 = new int[] { 0, 0, 0, 0, 0, 0, 7, 0, 0 };
            sudoku.Row7 = new int[] { 0, 0, 0, 5, 4, 0, 0, 0, 1 };
            sudoku.Row8 = new int[] { 0, 0, 0, 0, 3, 8, 0, 0, 0 };
            sudoku.SetCurrentCellsAsInmovable();

            Stopwatch stopwatch = Stopwatch.StartNew();
            solver.BruteForce(sudoku);
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);
        }

        private static void TestAssignMissingCells()
        {
            Sudoku sudoku = new Sudoku();
            int[] testnumbers = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            ConsoleTools.WriteSudokuLine(sudoku.Row8);
            sudoku.setMissingCellsInRow(8, testnumbers);
            ConsoleTools.WriteSudokuLine(sudoku.Row8);
        }

        private static void TestSolver()
        {
            Sudoku sudoku = new Sudoku();
            Solver solver = new Solver();
            sudoku.Row0 = new int[] { 8, 2, 7, 1, 5, 4, 3, 9, 6 };
            sudoku.Row1 = new int[] { 9, 6, 5, 3, 2, 7, 1, 4, 8 };
            sudoku.Row2 = new int[] { 3, 4, 1, 6, 8, 9, 7, 5, 2 };
            sudoku.Row3 = new int[] { 5, 9, 3, 4, 6, 8, 2, 7, 1 };
            sudoku.Row4 = new int[] { 4, 7, 2, 5, 1, 3, 6, 8, 9 };
            sudoku.Row5 = new int[] { 6, 1, 8, 9, 7, 2, 4, 3, 5 };
            sudoku.Row6 = new int[] { 7, 8, 6, 2, 3, 5, 9, 1, 4 };
            //sudoku.Row7 = new int[] { 1, 5, 4, 7, 9, 6, 8, 2, 3 };
            //sudoku.Row8 = new int[] { 2, 3, 9, 8, 4, 1, 5, 6, 7};


            for (int row = 0; row < 7; row++)
            {
                for (int column = 0; column < 9; column++)
                {
                    sudoku.SetInmovableCell(row, column);
                }
            }

            //sudoku.SetInmovableCell(8, 0, 1);
            //sudoku.SetInmovableCell(8, 1, 5);
            //sudoku.SetInmovableCell(8, 2, 4);
            //sudoku.SetInmovableCell(8, 3, 7);
            //sudoku.SetInmovableCell(8, 4, 9);
            //sudoku.SetInmovableCell(8, 5, 6);


            sudoku.SetInmovableCell(8, 0, 2);
            sudoku.SetInmovableCell(8, 1, 3);
            sudoku.SetInmovableCell(8, 2, 9);
            sudoku.SetInmovableCell(8, 3, 8);
            sudoku.SetInmovableCell(8, 4, 4);
            sudoku.SetInmovableCell(8, 5, 1);

            Stopwatch stopwatch = Stopwatch.StartNew();
            solver.BruteForce(sudoku);
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);

            Console.WriteLine("final shape is: ");
            ConsoleTools.WriteSudokuLine(sudoku.Row0);
            ConsoleTools.WriteSudokuLine(sudoku.Row1);
            ConsoleTools.WriteSudokuLine(sudoku.Row2);
            ConsoleTools.WriteSudokuLine(sudoku.Row3);
            ConsoleTools.WriteSudokuLine(sudoku.Row4);
            ConsoleTools.WriteSudokuLine(sudoku.Row5);
            ConsoleTools.WriteSudokuLine(sudoku.Row6);
            ConsoleTools.WriteSudokuLine(sudoku.Row7);
            ConsoleTools.WriteSudokuLine(sudoku.Row8);

            Console.WriteLine("sudoku is solved? " + sudoku.ValidateSudoku());
        }

        private static void TestPermutation()
        {

            Console.WriteLine("testing yield option");
            foreach (var item in PermutationCalculator.PermutationGenerator(new int[] { 1, 2, 3 }))
            {
                ConsoleTools.WriteSudokuLine(item);
            }


        }

        private static void TestSudoku()
        {
            Console.WriteLine("row test");

            Sudoku sudoku = new Sudoku();
            Solver solver = new Solver();

            ConsoleTools.WriteSudokuLine(sudoku.Row8);
            sudoku.Row8 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            ConsoleTools.WriteSudokuLine(sudoku.Row8);

            bool validation = sudoku.ValidateLine(sudoku.Row8);
            Console.WriteLine($"the validation was {validation}");

            validation = sudoku.ValidateLine(new int[] { 1, 2, 3, 3 });
            Console.WriteLine($"the validation was {validation}");

            Console.WriteLine($"about to validate the whole thing: ");
            validation = sudoku.ValidateSudoku();
            Console.WriteLine($"the validation was {validation}");

            int counter = 0;
            for (int i = 0; i < 9; i++)
            {
                int[] line = new int[9];
                for (int j = 0; j < 9; j++)
                {
                    line[j] = counter;
                    counter++;
                }

                Console.Write("craeting line: ");
                ConsoleTools.WriteSudokuLine(line);
                if (i == 0) { sudoku.Row0 = line; }
                if (i == 1) { sudoku.Row1 = line; }
                if (i == 2) { sudoku.Row2 = line; }
                if (i == 3) { sudoku.Row2 = line; }
                if (i == 4) { sudoku.Row5 = line; }
                if (i == 5) { sudoku.Row5 = line; }
                if (i == 6) { sudoku.Row6 = line; }
                if (i == 7) { sudoku.Row7 = line; }
                if (i == 8) { sudoku.Row8 = line; }

            }
            Console.WriteLine("set all numbers to be different");
            validation = sudoku.ValidateSudoku();
            Console.WriteLine($"the validation was {validation}");
        }

        /// <summary>
        /// when clicked the sudoku numbers get marked as the active cell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SelectCell(object sender, RoutedEventArgs e)
        {
            if (selectedCell != null)
            {
                selectedCell.Background = idleColor;
            }
            Button button = (Button)sender;
            button.Background = selectedColor;

            selectedCell = button;
        }

        private void Run_Solve(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Run Solve");
            Solver solver = new Solver();
            solver.BruteForce(mainSudoku);

            Button[][] allRows = GetButtonRows();

            /// set values to the ui to match the solved sudoku
            for (int row = 0; row < allRows.Length; row++)
            {
                Button[] currentRow = allRows[row];
                for (int column = 0; column < currentRow.Length; column++)
                {
                    if(mainSudoku.IndexIsInmovable(row, column))
                    {
                        continue;
                    }
                    Button button = currentRow[column];
                    button.Content = "" + mainSudoku.GetRow(row)[column];
                    button.Background = solveColor;
                }
            }
        }

        private void ButtonSetNumberOnGrid(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            int value = 0;

            if (button.Name == "buttonSet1")
            {
                value = 1;
            }
            else if (button.Name == "buttonSet2")
            {
                value = 2;
            }
            else if (button.Name == "buttonSet3")
            {
                value = 3;
            }
            else if (button.Name == "buttonSet4")
            {
                value = 4;
            }
            else if (button.Name == "buttonSet5")
            {
                value = 5;
            }
            else if (button.Name == "buttonSet6")
            {
                value = 6;
            }
            else if (button.Name == "buttonSet7")
            {
                value = 7;
            }
            else if (button.Name == "buttonSet8")
            {
                value = 8;
            }
            else if (button.Name == "buttonSet9")
            {
                value = 9;
            }

            if (value > 0)
            {
                Console.WriteLine(value);
                SetSelectedCell(value);
            }
        }

        /// <summary>
        /// set the value to the selected cell if legal, otherwise flash red.
        /// </summary>
        /// <param name="value"></param>
        private void SetSelectedCell(int value)
        {
            if (selectedCell == null)
            {
                return;
            }

            // get row/column from name; example name: Button3_7
            int row = (int)char.GetNumericValue(selectedCell.Name[6]);
            int column = (int)char.GetNumericValue(selectedCell.Name[8]);

            Sudoku testSudoku = new Sudoku(mainSudoku);
            testSudoku.SetInmovableCell(row, column, value);

            if (testSudoku.ValidateSudoku(ignoreZeroes: true))
            {
                mainSudoku.SetInmovableCell(row, column, value);
                selectedCell.Content = $"{value}";
            }
            else
            {
                FlashRed(selectedCell, testSudoku);

            }
        }

        /// <summary>
        /// at some point make a pretty animation flashing red.
        /// </summary>
        /// <param name="selectedCell"></param>
        private void FlashRed(Button selectedCell, Sudoku testSudoku)
        {
            Console.WriteLine("invalid cell. check for repeat numbers");
            foreach (var line in testSudoku.GetRows())
            {
                ConsoleTools.WriteSudokuLine(line);
            }
        }

        private Button[][] GetButtonRows()
        {
            Button[] row0 = new Button[] { Button0_0, Button0_1, Button0_2, Button0_3, Button0_4, Button0_5, Button0_6, Button0_7, Button0_8 };
            Button[] row1 = new Button[] { Button1_0, Button1_1, Button1_2, Button1_3, Button1_4, Button1_5, Button1_6, Button1_7, Button1_8 };
            Button[] row2 = new Button[] { Button2_0, Button2_1, Button2_2, Button2_3, Button2_4, Button2_5, Button2_6, Button2_7, Button2_8 };
            Button[] row3 = new Button[] { Button3_0, Button3_1, Button3_2, Button3_3, Button3_4, Button3_5, Button3_6, Button3_7, Button3_8 };
            Button[] row4 = new Button[] { Button4_0, Button4_1, Button4_2, Button4_3, Button4_4, Button4_5, Button4_6, Button4_7, Button4_8 };
            Button[] row5 = new Button[] { Button5_0, Button5_1, Button5_2, Button5_3, Button5_4, Button5_5, Button5_6, Button5_7, Button5_8 };
            Button[] row6 = new Button[] { Button6_0, Button6_1, Button6_2, Button6_3, Button6_4, Button6_5, Button6_6, Button6_7, Button6_8 };
            Button[] row7 = new Button[] { Button7_0, Button7_1, Button7_2, Button7_3, Button7_4, Button7_5, Button7_6, Button7_7, Button7_8 };
            Button[] row8 = new Button[] { Button8_0, Button8_1, Button8_2, Button8_3, Button8_4, Button8_5, Button8_6, Button8_7, Button8_8 };

            return new Button[][] { row0, row1, row2, row3, row4, row5, row6, row7, row8 };
        }
    }




}
